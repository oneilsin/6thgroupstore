
IF(OBJECT_ID('Categoria')IS NOT NULL)
BEGIN
    DROP TABLE Categoria 
    IF(OBJECT_ID('Producto')IS NOT NULL)
    BEGIN DROP TABLE Producto END
END
GO
CREATE TABLE Categoria(
    idCategoria INT IDENTITY NOT NULL
    , descripcion VARCHAR(50)
    , PRIMARY KEY(idCategoria)
);

IF(OBJECT_ID('Producto')IS NOT NULL)
BEGIN DROP TABLE Producto END
GO
CREATE TABLE Producto(
    idProducto INT IDENTITY NOT NULL
    , codigoProducto NVARCHAR(50) NOT NULL
    , idCategoria INT NOT NULL
    , nombreProducto VARCHAR(50) NOT NULL
    , precioBase DECIMAL(10,2) NOT NULL
    , stockMinimo INT NOT NULL
    , idUsuario INT NOT NULL
    , imageFile NVARCHAR(MAX) NULL
    , CHECK(stockMinimo>0)
    , PRIMARY KEY(idProducto)
    , FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
);


IF(OBJECT_ID('TipoMovimiento')IS NOT NULL)
BEGIN DROP TABLE TipoMovimiento END
GO
CREATE TABLE TipoMovimiento(
    idTipoMov INT IDENTITY NOT NULL
    , descripcion VARCHAR(100)
    , PRIMARY KEY(idTipoMov)
);

IF(OBJECT_ID('Inventario')IS NOT NULL)
BEGIN DROP TABLE Inventario END
GO
CREATE TABLE Inventario(
    idInventario BIGINT IDENTITY NOT NULL
    , fechaMov DATE NOT NULL
    , idTipoMov INT NOT NULL
    , flagMov CHAR(1) NOT NULL -- E/S entrada o salida para control de sotock
    , idProducto INT NOT NULL
    , cantidad DECIMAL(10,3) NOT NULL
    , condicion CHAR(1) NOT NULL --cuando el cliente cancela su pedido, se elimina los pendientes
    , comprobante NVARCHAR(30) NULL --aqui se guarda el documento de venta/slida y entrada/compra
    , CHECK(flagMov like'[ES]')
    , CHECK(condicion like'[RP]') -- R: Realizado, P: Pendiente
    , PRIMARY KEY(idInventario)
    , FOREIGN KEY(idTipoMov) REFERENCES TipoMovimiento(idTipoMov)
    , FOREIGN KEY(idProducto) REFERENCES Producto(idProducto)
);

IF(OBJECT_ID('Distrito')IS NOT NULL)
BEGIN DROP TABLE Distrito END
GO
CREATE TABLE Distrito(
    idDistrito NVARCHAR(6) NOT NULL
    , Descripcion VARCHAR(50) NOT NULL
    , PRIMARY KEY(idDistrito)
);

IF(OBJECT_ID('Persona')IS NOT NULL)
BEGIN DROP TABLE Persona END
GO
CREATE TABLE Persona(
    idPersona INT IDENTITY NOT NULL
    , dni NVARCHAR(12) NOT NULL
    , nombres VARCHAR(50) NOT NULL
    , apellidos VARCHAR(80) NULL
    , sexo CHAR(1) NULL
    , nacimiento DATE NULL
    , email VARCHAR(100) NOT NULL
    , celular NVARCHAR(9) NULL
    , idDistrito NVARCHAR(6) NULL
    , direccion VARCHAR(250) NULL
    , referencia VARCHAR(250) NULL
    , contraseña NVARCHAR(1700) NOT NULL
    , role VARCHAR(900) NOT NULL
    , UNIQUE(email)
    , UNIQUE(contraseña)
    , CHECK(sexo like'[HM]')
    , PRIMARY KEY(idPersona)
);

IF(OBJECT_ID('TipoComprobante')IS NOT NULL)
BEGIN DROP TABLE TipoComprobante END
GO
CREATE TABLE TipoComprobante(
    idTipoComp INT IDENTITY NOT NULL
    , descripcion VARCHAR(50) NOT NULL
    , nomenclatura VARCHAR(2) NOT NULL
    , PRIMARY KEY(idTipoComp)
);

IF(OBJECT_ID('Despacho')IS NOT NULL)
BEGIN DROP TABLE Despacho END
GO
CREATE TABLE Despacho(
    idDespacho INT IDENTITY NOT NULL
    , descripcion VARCHAR(30) NOT NULL
    , PRIMARY KEY(idDespacho)
);

IF(OBJECT_ID('Venta')IS NOT NULL)
BEGIN
    IF(OBJECT_ID('VentaDetalle')IS NOT NULL)
    BEGIN DROP TABLE VentaDetalle END
    DROP TABLE Venta END
GO
CREATE TABLE Venta(
    idVenta BIGINT IDENTITY NOT NULL
    , fechaVenta DATE NOT NULL
    , idTipoComp INT NOT NULL
    , comprobante NVARCHAR(13) NOT NULL --RC-2020100000
    , idCliente INT NOT NULL
    , importeTotal DECIMAL(10,2) NOT NULL
    , idVendedor INT NOT NULL
    , idDespacho INT NOT NULL
    , idUsuario INT NOT NULL
    , estado CHAR(1) NOT NULL 
    , compAnterior NVARCHAR(13) NULL
    , UNIQUE(comprobante)
    , CHECK(estado like'[VP]') --V:Vendido, P:Pendiente
    , PRIMARY KEY(idVenta)
    , FOREIGN KEY(idTipoComp) REFERENCES TipoComprobante(idTipoComp)
    , FOREIGN KEY(idCliente) REFERENCES Persona(idPersona)
    , FOREIGN KEY(idVendedor) REFERENCES Persona(idPersona)
    , FOREIGN KEY(idDespacho) REFERENCES Despacho(idDespacho)
);
-- ALTER TABLE Venta
-- Add UNIQUE(comprobante)
-- ALTER TABLE Venta
-- ADD compAnterior NVARCHAR(13)

IF(OBJECT_ID('VentaDetalle')IS NOT NULL)
BEGIN DROP TABLE VentaDetalle END
GO
CREATE TABLE VentaDetalle(
    idVentaDeta BIGINT IDENTITY NOT NULL
    , idVenta BIGINT NOT NULL
    , idProducto INT NOT NULL
    , cantidad DECIMAL(10,3) NOT NULL
    , costoUnitario DECIMAL(10,2) NOT NULL
    , costoTotal DECIMAL(10,2) NOT NULL
    , CHECK(cantidad>0)
    , UNIQUE(idProducto,idVenta)
    , PRIMARY KEY(idVentaDeta)
    , FOREIGN KEY(idVenta) REFERENCES Venta(idVenta)
    , FOREIGN KEY(idProducto) REFERENCES Producto(idProducto)
);

IF(OBJECT_ID('Contacto')IS NOT NULL)
BEGIN DROP TABLE Contacto END
GO
CREATE TABLE Contacto(
    idContacto BIGINT IDENTITY NOT NULL
    , nombre varchar(50) NOT NULL
    , email VARCHAR(100) NOT NULL
    , asunto VARCHAR(50) NOT NULL
    , mensaje varchar(4000) NOT NULL
    , PRIMARY KEY(idContacto)
);

IF(OBJECT_ID('Oferta')IS NOT NULL)
BEGIN DROP TABLE Oferta END
GO
CREATE TABLE Oferta(
    idOferta INT IDENTITY NOT NULL
    , descripcion VARCHAR(100) NOT NULL
    , costoOferta DECIMAL(10,2) NOT NULL
    , vigencia BIT NOT NULL
    , idUsuario INT NOT NULL
    , PRIMARY KEY(idOferta)
);

IF(OBJECT_ID('OfertaDetalle')IS NOT NULL)
BEGIN DROP TABLE OfertaDetalle END
GO
CREATE TABLE OfertaDetalle(
    idOfertaDeta BIGINT IDENTITY NOT NULL
    , idOferta INT NOT NULL
    , IdProducto INT NOT NULL
    , cantidad DECIMAL(10,3) NOT NULL
    , costoUnitario DECIMAL(10,2) NOT NULL
    , costoTotal DECIMAL(10,2) NOT NULL
    , PRIMARY KEY(idOfertaDeta)
    , FOREIGN KEY(idOferta) REFERENCES Oferta(idOferta)
    , FOREIGN KEY(idProducto) REFERENCES Producto(idProducto)
);