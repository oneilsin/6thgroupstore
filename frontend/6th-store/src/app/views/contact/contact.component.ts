import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

// contactForm = new FormGroup({
//   nameClient: new FormControl(''),
//   email: new FormControl(''),
//   subject: new FormControl(''),
//   message: new FormControl('')
// });

contactForm = this.fb.group({
  nameClient: ['', [Validators.required, Validators.minLength(3)] ],
  email: ['', [Validators.required, Validators.email] ],
  subject: ['', [Validators.required, Validators.minLength(5)] ],
  message: ['']
});
// tambien hay validaciones complejar para cotraseñas

  constructor(private fb: FormBuilder) { }

  onSubmit(){
    if(this.contactForm.valid){
      console.log(this.contactForm.value)
    }else{
      alert("Los campos requeridos no tienen datos, por favor intente nuevamente.");
    }
  }

  ngOnInit(): void {
  }

}
