import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/service/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  categories = [];

  constructor(private readonly homeService: HomeService) { }

  getCategories(){
    this.homeService.getBannerCategory().subscribe((rest:any)=>{
      this.categories=rest.data;
      console.log(this.categories);
    })
  }

  ngOnInit(): void {
    this.getCategories();
  }

}
