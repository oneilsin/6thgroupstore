import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private readonly http: HttpClient) { }

  //Crear funcion de las apis.
  getBannerCategory() {
    return this.http.get('/api/home/GetHome');
  }
}
