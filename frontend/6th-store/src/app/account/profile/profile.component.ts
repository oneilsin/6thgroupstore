import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileForm = this.fb.group({
    people: this.fb.group({
      nameUser: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      dni: ['', [Validators.required, Validators.minLength(8)]],
      birth: [''],
      gender: [''],
    }),

    email: ['', [Validators.required, Validators.email]],
    password1: ['', Validators.required],
    password2: ['', Validators.required],
    phone: ['', [Validators.required, Validators.minLength(9)]],
    district: [null, [Validators.required]],
    address: ['', [Validators.required]],
    ref: [''],
    errorMessage: ['']
  });

  districts = ["Ate","Lima","San Borja","Chorrillos","Rimac","San Juan de Lurigancho","San Juan de Miraflores","Canto Grande"]

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.updateValues()
  }

  updateValues(){
    this.profileForm.patchValue({
      people: {
        nameUser: 'Juan',
        lastName: 'Pérez Prado',
        dni: '30303030',
        birth: '10/02/1990',
        gener: 'H',
      },
      email: 'jperez@6thmail.com',
      password1: 'abc123',
      password2: 'abc123',
      phone: '900990000',
      district: 'Ate',
      address: 'Calle los laureles 240',
      ref: 'a 2 cuadras de hipermercado Metro'
    });
  }

  onSubmit(){
    if(this.profileForm.valid){
      console.log(this.profileForm.value)
    }else{
      alert("Los campos requeridos no tienen datos, por favor intente nuevamente.");
    }
  }

  dpay: boolean = false;
  togglePay() {
    this.dpay = !this.dpay;
    if (this.dpay) {
      this.dcont = !this.dpay;
      this.dprof = !this.dpay;
    }
    else {
      this.dpay = true;
    }
  }

  dprof: boolean = true;
  toggleProf() {
    this.dprof = !this.dprof;
    if (this.dprof) {
      this.dcont = !this.dprof;
      this.dpay = !this.dprof;
    }
    else {
      this.dprof = true;
    }
  }

  dcont: boolean = false;
  toggleCont() {
    this.dcont = !this.dcont;
    if (this.dcont) {
      this.dprof = !this.dcont;
      this.dpay = !this.dcont;
    }
    else {
      this.dcont = true;
    }
  }

  //Ver: para luego trabajar
  errorsms: boolean=false;
  showError(){
    if(!this.profileForm.valid){
      this.errorsms=true;
    }else{
      this.errorsms=false;
    }

    // this.errorsms=!this.errorsms;
    // if(this.errorsms){
    //   // this.errorMessage=
    // }
  }



}
