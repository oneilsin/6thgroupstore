const PROXY_CONFIG = [{
  context: ['/api'], //Contexto para llamar a la api
  target: 'http://localhost:4000/',
  secure: false,
},];

module.exports = PROXY_CONFIG;
