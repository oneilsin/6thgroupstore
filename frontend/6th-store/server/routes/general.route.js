//indicamos que archivo se debe consumir.. los end point.
module.exports = app=>{
  app.get('/api/home/GetHome',(req, res)=>{
    var data = require('../json/home.json');
    res.json(data);
  })

  app.get('/api/category/GetCategory', (req, res)=>{
    var datas = require('../json/category.json');
    res.json(datas);
  })
}
