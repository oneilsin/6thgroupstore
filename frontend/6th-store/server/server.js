const express = require('express'); //constante:
const port = 4000; //Puerto para API

const app = express(); //Levantar la app.

//Add permisos de cualquier invocación.
app.all('*', function(req, res, next){
  res.header('Access-Control-Allow-Origin','*');
  res.header('Access-Control-Allow-Methods','PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers','Content-Type');
  next();
})

//llamar al enrutador.
require('./routes/general.route')(app);

//crear item; listening peticiones
app.listen(port,()=>{
  console.log('Init Server ... '+ port);
})
